#include <sys/signal.h>

int sigsuspend(const sigset_t *mask)
{
	return 0;
}

int sigaction(int signum, const struct sigaction *act,
                     struct sigaction *oldact)
{
	return 0;
}

int sigprocmask(int how, const sigset_t *set, sigset_t *oldset)
{
	return 0;
}
