#include <sys/unistd.h>

long sysconf(int name)
{
	switch (name) {
	case _SC_PAGESIZE:
		return 4096;
	default:
		return -1;
	}

}

char *program_invocation_short_name = "?";
