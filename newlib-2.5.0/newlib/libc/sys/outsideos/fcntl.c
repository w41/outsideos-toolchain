#include <sys/fcntl.h>
#include <stdarg.h>

int fcntl(int fd, int cmd, ...)
{
	va_list ap;
	int res;

	va_start(ap, cmd);
	res = _fcntl(fd, cmd, va_arg(ap, int));
	va_end(ap);
	return res;
}

int open(const char *name, int flags, ...)
{
	va_list ap;
	int res;

	va_start(ap, flags);
	res = _open(name, flags, va_arg(ap, int));
	va_end(ap);
	return res;
}
