#ifndef _SYS_UTMP_H
#define _SYS_UTMP_H

#include <sys/types.h>

#define UTMP_FILE "/var/utmp"
#define WTMP_FILE "/var/wtmp"

#define UT_LINESIZE	32
#define UT_NAMESIZE	32
#define UT_HOSTSIZE	256

struct utmp {
   short   ut_type;              /* Type of record */
   pid_t   ut_pid;               /* PID of login process */
   char    ut_line[UT_LINESIZE]; /* Device name of tty - "/dev/" */
   char    ut_id[4];             /* Terminal name suffix,
				    or inittab(5) ID */
   char    ut_user[UT_NAMESIZE]; /* Username */
   char    ut_host[UT_HOSTSIZE]; /* Hostname for remote login, or
				    kernel version for run-level
				    messages */
   struct  exit_status {
	   short e_termination;
	   short e_exit;
   } ut_exit;  /* Exit status of a process
				    marked as DEAD_PROCESS; not
				    used by Linux init(1) */
   /* The ut_session and ut_tv fields must be the same size when
      compiled 32- and 64-bit.  This allows data files and shared
      memory to be shared between 32- and 64-bit applications. */
#if __WORDSIZE == 64 && defined __WORDSIZE_COMPAT32
   int32_t ut_session;           /* Session ID (getsid(2)),
				    used for windowing */
   struct {
       int32_t tv_sec;           /* Seconds */
       int32_t tv_usec;          /* Microseconds */
   } ut_tv;                      /* Time entry was made */
#else
    long   ut_session;           /* Session ID */
    struct timeval ut_tv;        /* Time entry was made */
#endif

   int32_t ut_addr_v6[4];        /* Internet address of remote
				    host; IPv4 address uses
				    just ut_addr_v6[0] */
//   char __unused[20];            /* Reserved for future use */
};

/* Backward compatibility hacks */
#define ut_name ut_user
#ifndef _NO_UT_TIME
#define ut_time ut_tv.tv_sec
#endif
#define ut_xtime ut_tv.tv_sec
#define ut_addr ut_addr_v6[0]

#define RUN_LVL		1
#define BOOT_TIME	2
#define NEW_TIME	3
#define OLD_TIME	4

#define INIT_PROCESS	5
#define LOGIN_PROCESS	6
#define USER_PROCESS	7
#define DEAD_PROCESS	8


/* --- redundant, from sys/cygwin/sys/utmp.h --- */

struct utmp *_getutline (struct utmp *);
struct utmp *getutent (void);
struct utmp *getutid (struct utmp *);
struct utmp *getutline (struct utmp *);
void endutent (void);
struct utmp *pututline(const struct utmp *ut);
void setutent (void);
void utmpname (const char *);

#endif
