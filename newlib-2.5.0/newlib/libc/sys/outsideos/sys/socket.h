#ifndef _SYS_SOCKET_H
#define	_SYS_SOCKET_H

typedef int socklen_t;
#define __socklen_t_defined

#define SOCK_STREAM 1
#define SOCK_DGRAM 2

typedef uint8_t sa_family_t;

struct sockaddr {
	sa_family_t	sa_family;	/* address family */
	char		sa_data[14];	/* actually longer; address value */
};
#define	SOCK_MAXADDRLEN	255		/* longest possible addresses */

/* Protocol families.  */
#define AF_UNSPEC       0
#define AF_LOCAL        1
#define AF_INET         2


#endif
