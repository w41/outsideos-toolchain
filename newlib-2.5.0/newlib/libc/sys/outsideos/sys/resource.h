#ifndef _SYS_RESOURCE_H
#define _SYS_RESOURCE_H

#include <sys/time.h>

#define RLIMIT_CPU			0	/* CPU time in sec */
#define RLIMIT_FSIZE		1	/* Maximum filesize */
#define RLIMIT_DATA			2	/* Max data size */
#define RLIMIT_STACK		3	/* Max stack size */
#define RLIMIT_CORE			4	/* Max core file size */
#define RLIMIT_RSS			5	/* Max resident set size */
#define RLIMIT_NPROC		6	/* Max number of processes */
#define RLIMIT_NOFILE		7	/* Max number of open files */
#define RLIMIT_OFILE		RLIMIT_NOFILE
#define RLIMIT_MEMLOCK		8	/* Max locked-in-memory address space */
#define RLIMIT_AS			9	/* Address space limit */
#define RLIMIT_LOCKS		10	/* Maximum file locks held */
#define RLIMIT_SIGPENDING	11	/* Max number of pending signals */
#define RLIMIT_MSGQUEUE		12	/* Maximum bytes in POSIX mqueues */
#define RLIMIT_NICE			13	/* Max nice prio allowed to raise to 0-39 for nice level 19 .. -20 */
#define RLIMIT_RTPRIO		14	/* Maximum realtime priority */
#define RLIMIT_RTTIME		15	/* Timeout for RT tasks in us */
#define RLIM_NLIMITS		16

#ifndef RLIM_INFINITY
#define RLIM_INFINITY		(~0UL)
#endif

typedef unsigned long rlim_t;

struct rlimit {
	rlim_t rlim_cur;
	rlim_t rlim_max;
};

#define	RUSAGE_SELF	0		/* calling process */
#define	RUSAGE_CHILDREN	-1		/* terminated child processes */

struct rusage {
  	struct timeval ru_utime;	/* user time used */
	struct timeval ru_stime;	/* system time used */
};

int	_EXFUN(getrusage, (int, struct rusage*));
int setrlimit(int resource, const struct rlimit *rlim);

#endif
