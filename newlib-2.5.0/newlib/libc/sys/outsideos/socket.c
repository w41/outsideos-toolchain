#include <netdb.h>
#include <sys/socket.h>

int             getaddrinfo(const char *__restrict, const char *__restrict,
                            const struct addrinfo *__restrict,
                            struct addrinfo **__restrict)
{
	return 0;
}

int             getnameinfo(const struct sockaddr *__restrict, socklen_t,
                            char *__restrict, socklen_t, char *__restrict,
                            socklen_t, unsigned int)
{
	return 0;
}

void            freeaddrinfo(struct addrinfo *)
{
}

char            *gai_strerror(int)
{
	return "";
}

int             setnetgrent(const char *)
{
	return 0;
}

void            setservent(int)
{
}
