#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdarg.h>

int mkfifo(const char *path, mode_t mode)
{
	return 0;
}

int ioctl(int fd,int request,...)
{
    va_list ap;
    int res;

    va_start(ap,request);
    res = _ioctl(fd,request,va_arg(ap,void *));
    va_end(ap);
    return res;
}
