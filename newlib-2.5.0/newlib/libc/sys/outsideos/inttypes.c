#include <stdlib.h>
#include <inttypes.h>

uintmax_t strtoumax(const char *__restrict nptr, char **__restrict endptr, int base)
{
	return strtoull(nptr, endptr, base);
}
