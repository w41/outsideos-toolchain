#include <sys/termios.h>
#include <sys/ioctl.h>
#include <sys/syscall.h>
#include <sys/types.h>

int tcgetattr(int fd,struct termios *termios_p)
{
	return ioctl(fd, TCGETS, termios_p);
}

int tcsetattr(int fd,int optional_actions,const struct termios *termios_p)
{
	return 0;
}

int tcflow (int fd, int action)
{
	return 0;
}

int
tcflush (int fd, int queue_selector)
{
	return 0;
}

pid_t tcgetpgrp(int fd)
{
	return 0;
}

