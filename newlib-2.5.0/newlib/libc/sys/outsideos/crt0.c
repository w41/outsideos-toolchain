#include <fcntl.h>
 
extern void exit(int code);
extern int main(int argc, char *argv[], char *envp[]);

static int get_argc(char *argv[])
{
	int retval = 0;
	while (argv[retval]) retval++;

	return retval;
}
 
void _start(char *argv[], char *envp[]) {
    int ex = main(get_argc(argv), argv, envp);
    exit(ex);
}
