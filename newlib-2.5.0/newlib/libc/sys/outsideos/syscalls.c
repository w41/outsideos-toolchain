/* note these headers are all provided by newlib - you don't need to provide them */
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/times.h>
#include <sys/errno.h>
#include <sys/time.h>
#include <sys/dirent.h>
#include <stdio.h>

#include "sys/syscall.h"

/* Requires saving %ebx because errno is a function which will clobber %eax. */
#define CONVERT_TO_ERRNO() \
	asm("test %eax, %eax"); asm("jge 1f"); asm("neg %eax"); asm("mov %eax, %ebx"); asm("mov %%ebx, %0" : "=m"(errno)); asm("mov %0, %%eax" : : "i"(-1)); asm("1: ")
 
void _exit(int status) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_exit)); asm("mov %0, %%ebx" : : "X"(status)); asm("int $0x80"); asm("pop %ebx"); }
int close(int file) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_close)); asm("mov %0, %%ebx" : : "X"(file)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ebx"); }
extern char **environ; /* pointer to array of char * strings that define the current environment variables */
int _execve(char *name, char **argv, char **env) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_execve)); asm("mov %0, %%ebx" : : "X"(name)); asm("mov %0, %%ecx" : : "X"(argv)); asm("mov %0, %%edx" : : "X"(env)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
int fork() { asm("mov %0, %%eax" : : "i"(SYSCALL_fork)); asm("int $0x80"); }
int fstat(int file, struct stat *st) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_fstat)); asm("mov %0, %%ebx" : : "X"(file)); asm("mov %0, %%ecx" : : "X"(st)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int sync(void) { asm("mov %0, %%eax" : : "i"(SYSCALL_sync)); asm("int $0x80"); }
int fsync(int fd) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_fsync)); asm("mov %0, %%ebx" : : "X"(fd)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ebx"); }
int getpid() { asm("mov %0, %%eax" : : "i"(SYSCALL_getpid)); asm("int $0x80"); }
int getrlimit(int resource, struct rlimit *rlim) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_getrlimit)); asm("mov %0, %%ebx" : : "X"(resource)); asm("mov %0, %%ecx" : : "X"(rlim)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int setrlimit(int resource, const struct rlimit *rlim) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_setrlimit)); asm("mov %0, %%ebx" : : "X"(resource)); asm("mov %0, %%ecx" : : "X"(rlim)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int _isatty(int file) { return file == 0 || file == 1 || file == 2; }
int kill(pid_t pid, int sig) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_kill)); asm("mov %0, %%ebx": : "X"(pid)); asm("mov %0, %%ecx" : : "X"(sig)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int link(char *old, char *new) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_link)); asm("mov %0, %%ebx": : "X"(old)); asm("mov %0, %%ecx" : : "X"(new)); asm("int $0x80"); asm("pop %ecx"); asm("pop %ebx"); }
int lseek(int file, int ptr, int dir) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_lseek)); asm("mov %0, %%ebx" : : "X"(file)); asm("mov %0, %%ecx" : : "X"(ptr)); asm("mov %0, %%edx" : : "X"(dir)); asm("int $0x80"); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
int mknod(const char *pathname, mode_t mode, dev_t dev) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_mknod)); asm("mov %0, %%ebx" : : "X"(pathname)); asm("mov %0, %%ecx" : : "X"(mode)); asm("mov %0, %%dx" : : "X"(dev)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
int _open(const char *name, int flags, mode_t mode) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_open)); asm("mov %0, %%ebx" : : "X"(name)); asm("mov %0, %%ecx" : : "X"(flags)); asm("mov %0, %%dx" : : "X"(mode)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx");  }
int read(int file, char *ptr, int len) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_read)); asm("mov %0, %%ebx" : : "X"(file)); asm("mov %0, %%ecx" : : "X"(ptr)); asm("mov %0, %%edx" : : "X"(len)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
caddr_t sbrk(int incr) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_brk)); asm("mov %0, %%ebx" : : "X"(incr)); asm("int $0x80"); asm("pop %ebx"); }
int stat(const char *file, struct stat *st) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_stat)); asm("mov %0, %%ebx" : : "X"(file)); asm("mov %0, %%ecx" : : "X"(st)); asm("int $0x80"); asm("pop %ecx"); asm("pop %ebx"); }
clock_t times(struct tms *buf) { return 0; }
int unlink(char *name) { return 0; }
int waitpid(pid_t pid, int *status, int options) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_waitpid)); asm("mov %0, %%ebx" : : "X"(pid)); asm("mov %0, %%ecx" : : "X"(status)); asm ("mov %0, %%edx" : : "X"(options)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
int wait(int *status) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_waitpid)); asm("mov %0, %%ebx" : : "i"(-1)); asm("mov %0, %%ecx" : : "X"(status)); asm ("mov %0, %%edx" : : "i"(0)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
int write(int file, char *ptr, int len) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_write)); asm("mov %0, %%ebx" : : "X"(file)); asm("mov %0, %%ecx" : : "X"(ptr)); asm("mov %0, %%edx" : : ""(len)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
int gettimeofday(struct timeval * restrict p, void *z) { return 0; }


int _ioctl(int fd, int request, void *arg) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_ioctl)); asm("mov %0, %%ebx" : : "X"(fd)); asm("mov %0, %%ecx" : : "X"(request)); asm("mov %0, %%edx" : : "X"(arg)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }

int pipe(int pipefd[2]) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_pipe)); asm("mov %0, %%ebx" : : "X"(pipefd)); asm("int $0x80"); asm("pop %ebx"); }
int alarm(unsigned int seconds) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_alarm)); asm("mov %0, %%ebx" : : "X"(seconds)); asm("int $0x80"); asm("pop %ebx"); }
int pause(void) { asm("mov %0, %%eax" : : "i"(SYSCALL_pause)); asm("int $0x80"); CONVERT_TO_ERRNO(); }
int getppid() { asm("mov %0, %%eax" : : "i"(SYSCALL_getppid)); asm("int $0x80"); }
int chdir(const char *path) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_chdir)); asm("mov %0, %%ebx" : : "X"(path)); asm("int $0x80"); asm("pop %ebx"); }
mode_t umask(mode_t mask) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_umask)); asm("mov %0, %%ebx" : : "X"(mask)); asm("int $0x80"); asm("pop %ebx"); }
int access(const char *filename, int mode) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_access)); asm("mov %0, %%ebx" : : "X"(filename)); asm("mov %0, %%ecx" : : "X"(mode)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int getdents(unsigned int fd, struct dirent *dirent, unsigned int count) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_getdents)); asm("mov %0, %%ebx" : : "X"(fd)); asm("mov %0, %%ecx" : : "X"(dirent)); asm("mov %0, %%edx" : : ""(count)); asm("int $0x80"); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout) { return 0; }
int sleep(int seconds) { return 0; }

int setuid(uid_t uid) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_setuid)); asm("mov %0, %%bx" : : "X"(uid)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ebx"); }
int setgid(gid_t gid) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_setgid)); asm("mov %0, %%bx" : : "X"(gid)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ebx"); }
pid_t setsid(void) { asm("mov %0, %%eax" : : "i"(SYSCALL_setsid)); asm("int $0x80"); CONVERT_TO_ERRNO(); }
int setreuid(uid_t ruid, uid_t euid) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_setreuid)); asm("mov %0, %%bx" : : "X"(ruid)); asm("mov %0, %%cx" : : "X"(euid)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int setregid(gid_t rgid, gid_t egid) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_setregid)); asm("mov %0, %%bx" : : "X"(rgid)); asm("mov %0, %%cx" : : "X"(egid)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int setpgid(pid_t pid, gid_t gid) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_setpgid)); asm("mov %0, %%bx" : : "X"(pid)); asm("mov %0, %%cx" : : "X"(gid)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }


uid_t getuid() { asm("mov %0, %%eax" : : "i"(SYSCALL_getuid)); asm("int $0x80"); CONVERT_TO_ERRNO(); }
uid_t getgid() { asm("mov %0, %%eax" : : "i"(SYSCALL_getgid)); asm("int $0x80"); CONVERT_TO_ERRNO(); }
uid_t geteuid() { asm("mov %0, %%eax" : : "i"(SYSCALL_geteuid)); asm("int $0x80"); CONVERT_TO_ERRNO(); }
uid_t getegid() { asm("mov %0, %%eax" : : "i"(SYSCALL_getegid)); asm("int $0x80"); CONVERT_TO_ERRNO(); }

int dup(int oldfd) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_dup)); asm("mov %0, %%ebx" : : "X"(oldfd)); asm("int $0x80"); asm("pop %ebx"); }
int dup2(int oldfd, int newfd) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_dup2)); asm("mov %0, %%ebx" : : "X"(oldfd)); asm("mov %0, %%ecx" : : "X"(newfd)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int chmod(const char *path, mode_t mode) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_chmod)); asm("mov %0, %%ebx" : : "X"(path)); asm("mov %0, %%ecx" : : "X"(mode)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int fchmod(int fd, mode_t mode) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_fchmod)); asm("mov %0, %%ebx" : : "X"(fd)); asm("mov %0, %%ecx" : : "X"(mode)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int fchown(int fd, uid_t owner, gid_t group) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_fchown)); asm("mov %0, %%ebx" : : "X"(fd)); asm("mov %0, %%cx" : : "X"(owner)); asm("mov %0, %%dx" : : "X"(group)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
int truncate(const char *path, off_t length) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_truncate)); asm("mov %0, %%ebx" : : "X"(path)); asm("mov %0, %%ecx" : : "X"(length)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int ftruncate(int fd, off_t length) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_ftruncate)); asm("mov %0, %%ebx" : : "X"(fd)); asm("mov %0, %%ecx" : : "X"(length)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }

int utime(const char *filename, const struct utimbuf *times) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_utime)); asm("mov %0, %%ebx" : : "X"(filename)); asm("mov %0, %%ecx" : : "X"(times)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx"); }
int mkdir(const char *pathname, mode_t mode) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_mkdir)); asm("mov %0, %%ebx" : : "X"(pathname)); asm("mov %0, %%ecx" : : "X"(mode)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx");  }
int rmdir(const char *pathname) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_rmdir)); asm("mov %0, %%ebx" : : "X"(pathname)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ebx"); }

int statfs(const char *path, struct statfs *buf) { asm("push %ebx"); asm("push %ecx"); asm("mov %0, %%eax" : : "i"(SYSCALL_statfs)); asm("mov %0, %%ebx" : : "X"(path)); asm("mov %0, %%ecx" : : "X"(buf)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ecx"); asm("pop %ebx");  }
int uname(struct utsname *utsname) { asm("push %ebx"); asm("mov %0, %%eax" : : "i"(SYSCALL_uname)); asm("mov %0, %%ebx" : : "X"(utsname)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %ebx"); }
int _fcntl(int fd, int cmd, int arg) { asm("push %ebx"); asm("push %ecx"); asm("push %edx"); asm("mov %0, %%eax" : : "i"(SYSCALL_fcntl)); asm("mov %0, %%ebx" : : "X"(fd)); asm("mov %0, %%ecx" : : "X"(cmd)); asm("mov %0, %%edx" : : "X"(arg)); asm("int $0x80"); CONVERT_TO_ERRNO(); asm("pop %edx"); asm("pop %ecx"); asm("pop %ebx"); }
