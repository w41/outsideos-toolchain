class A
{
	public:
	const char *getstr() const noexcept {
		return str;
	};
	private:
		char str[10];
};

int f(char *b)
{
	b[0] = '0';
}

int main()
{
	A a;
	const_cast<char *>(a.getstr())[0] = 'A';

	const char* b = "abcde";
}
