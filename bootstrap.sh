if [ -z "$OUTSIDEOS_ROOT" ]; then
	echo "Cannot continue: variable OUTSIDEOS_ROOT not defined."
	exit 1
fi

mkdir sysroot

echo "Installing autoconf..."
mkdir -p build/autoconf
cd build/autoconf
MAKEINFO=true ../../autoconf-2.65/configure --prefix=`realpath ../..`/install 2>&1 1>/dev/null
make -s -j8 && make -s install 2>&1 1>/dev/null
cd ../..

echo "Installing automake..."
mkdir -p build/automake
cd build/automake
MAKEINFO=true ../../automake-1.11/configure --prefix=`realpath ../..`/install 2>&1 1>/dev/null
make -s -j8 && make -s install 2>&1 1>/dev/null
cd ../..

echo "Copying glibc headers..."
mkdir -p sysroot/usr/include
cp -r newlib-2.5.0/newlib/libc/include/* sysroot/usr/include

echo "Copying some kernel headers into newlib..."
cp -r ${OUTSIDEOS_ROOT}/syscall.h newlib-2.5.0/newlib/libc/sys/outsideos/sys

echo "Installing cross-binutils..."
mkdir -p build/binutils
cd build/binutils
../../binutils-gdb/configure --prefix=`realpath ../..`/install --target=i686-outsideos --with-sysroot --disable-nls --disable-gdb --enable-binutils --with-sysroot=`realpath ../..`/sysroot --enable-quiet-rules 2>&1 1>/dev/null
make -s -j8 && make -s install 2>&1 1>/dev/null
cd ../..

echo "Installing cross-gcc..."
mkdir -p build/gcc
cd build/gcc
../../gcc/configure --prefix=`realpath ../..`/install --target=i686-outsideos --disable-nls --enable-languages=c --with-sysroot=`realpath ../..`/sysroot 2>&1 1>/dev/null
make -s -j8 all-gcc 2>&1 1>/dev/null
make -s -j8 all-target-libgcc 2>&1 1>/dev/null
make -s install-gcc 2>&1 1>/dev/null
make -s install-target-libgcc 2>&1 1>/dev/null
cd ../..

echo "Running autoconf + automake..."
cd newlib-2.5.0/newlib/libc/sys/outsideos
../../../../../install/bin/autoreconf 2>&1 1>/dev/null
../../../../../install/bin/automake 2>&1 1>/dev/null
cd ..
../../../../install/bin/automake 2>&1 1>/dev/null
cd ../../../../

echo "Installing native newlib..."
mkdir -p build/newlib
cd build/newlib
../../newlib-2.5.0/configure --prefix=/usr --target=i686-outsideos --enable-newlib-multithread=no --enable-pthread=no --disable-newlib-fvwrite-in-streamio 2>&1 1>/dev/null
make -s -j8
make DESTDIR=`realpath ../../`/sysroot install
cp -r `realpath ../..`/sysroot/usr/i686-outsideos/include `realpath ../..`/sysroot/usr
cp -r `realpath ../..`/sysroot/usr/i686-outsideos/lib `realpath ../..`/sysroot/usr
cd ../..
